## Spring Cloud Configuration Server

The Spring Cloud configuration server is a REST-based application that’s built on top
of Spring Boot which helps you to manage configurations and decouple configuration management
from your code.

### How to use it?

* **Clone** it using below url:

        git clone https://ankitpandoh@bitbucket.org/ankitpandoh/config-server.git

* **Create GIT Repository** to store configurations. For e.g. say you have two microservices named as ```mserviceA``` and ```mserviceB```. Each microservice
can have configuration files per environment (dev, test and prod). The naming convention for the configuration file are ```appname-env.yml``` The structure might look like this:
        
        config-repo
        |- mserviceA
           |- mserviceA.yml  ## This is default file.
           |- mserviceA-dev.yml
           |- mserviceA-test.yml
           |- mserviceA-prod.yml
        |- mserviceB
           |- mserviceB.yml
           |- mserviceB-dev.yml
           |- mserviceB-test.yml
           |- mserviceB-prod.yml
           
**Note** : If you do not run your spring boot microservice application under any profile. In that case default configuration file will be picked up. 
         
* **Update** ```application.yml```

        server:
         port: <port>   ## Port at which your config server is running
        spring:
          cloud:
           config:
            server:
             git:
              uri: https://github.com/somepath/config-repo  ## Git Path to your configuration repository
              searchPaths: mserviceA, mserviceB  ## relative path where config server will look for configurations

* **Test** it. Once you run your application. Use Postman or browser:

        http://<server>:<port>/mserviceA/dev
        
 JSON payload being returned with all of properties contained within the mserviceA-dev.
 yml file and mserviceA.yml (Default).
        
        {
            "name": "mserviceA",
            "profiles": [
                "dev"
            ],
            "label": null,
            "version": "0570592fec03ad7596fee8cb4b00fca3576241be",
            "state": null,
            "propertySources": [
                {
                    "name": "https://github.com/somepath/config-repo/mserviceA/mserviceA-dev.yml",
                    "source": {
                        "message": "Dev MService A says Hello"
                    }
                },
                {
                    "name": "https://github.com/somepath/config-repo/mserviceA/mserviceA.yml",
                    "source": {
                        "message": "Default MService A says Hello"
                    }
                }
            ]
        }
        
## Setting up the Client (Microservice) to use Config Server

After setting up the Config Server, you need to tell your  microservice application where to contact the Config Server. The ```bootstrap.yml``` file reads the application properties before any other configuration information used.
In general, the ```bootstrap.yml``` file contains the **application name** for the service, the **application profile**, and the **URI to connect to a Spring Cloud Config server**. Any other configuration information that you want to keep local
to the service (and not stored in Spring Cloud Config) can be set locally in the services in the ```application.yml``` file. Usually, the information you store in the ```application.yml``` file is configuration data that you might want to have available to a service even 
if the Spring Cloud Config service is unavailable. Both the ```bootstrap.yml``` and ```application.yml``` files are stored in a projects ```src/main/resources``` directory.

**bootstrap.yml**

        spring:
         application:
          name: mServiceA
         profiles:
          active: dev
         cloud:
          config:
           uri: http://<server>:<port>

In order to reflect properties change dynamically without restarting the client use ```@RefreshScope``` on your configuration classes. Following changes are required:

**pom.xml**
        
        <dependency>
        	<groupId>org.springframework.boot</groupId>
        	<artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>

**application.properties**
    
        management:
         endpoints:
          web:
           exposure:
            include: "*"
       
After updating the properties, use below command to refresh:
    
        curl http://<server>:<port>/actuator/refresh -d {} -H "Content-Type: application/json"
